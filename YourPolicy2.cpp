#include "YourPolicy2.hpp"
#include "MemoryStructure.hpp"
#include "ScheduleProcessor.hpp"
#include "defines.hpp"
#include <thread>
#include <vector>
#include <algorithm>
#include <iostream>

static int defragmentPrepareEnd(int target_size);
static int defragmentPrepareFront(int target_size);
static bool orderByRemainder(MemoryCandidate mc1, MemoryCandidate mc2);
static int classOf(int size);

static int prevSize = 0;

// best fit
void *YourPolicy2::onMalloc(int size)
{
  // Here to be implemented
  MemoryStructure *ms = MemoryStructure::getInstance();
  bool ignore = false;

  while (true)
  {
    void *prev_end = 0;
    std::vector<MemoryCandidate> candidates;

    for (int i = 0; i < ms->getAllocationListSize(); i++)
    {
      Allocation alloc = ms->getAllocation(i);
      int freeSize = (size_t)alloc.addr - (size_t)prev_end;
      if (freeSize >= size && (classOf(freeSize) == classOf(size) || ignore))
      {
        MemoryCandidate mc = MemoryCandidate();
        mc.beginAddr = prev_end;
        mc.size = size;
        mc.remainder = (size_t)alloc.addr - (size_t)prev_end - (size_t)size;
        candidates.push_back(mc);
      }

      prev_end = (void *)((size_t)alloc.addr + (size_t)alloc.size);
    }
    int freeSize = MAX_MEMORY_CAP - (size_t)prev_end;
    if (freeSize >= size && (classOf(freeSize) == classOf(size) || ignore))
    {
      MemoryCandidate mc = MemoryCandidate();
      mc.beginAddr = prev_end;
      mc.size = size;
      mc.remainder = MAX_MEMORY_CAP - (size_t)prev_end - (size_t)size;
      candidates.push_back(mc);
    }

    if (candidates.size() != 0)
    {
      ignore = false;
      sort(candidates.begin(), candidates.end(), orderByRemainder);
      ASSERT(ms->allocate(candidates[0].beginAddr, candidates[0].size));
      prevSize = candidates[0].size;
      return candidates[0].beginAddr;
    }
    if (!ignore)
    {
      ignore = true;
      if (prevSize == size)
        continue;
    }
    if (!defragmentPrepareEnd(size))
      defragmentPrepareFront(size);
  }
}

void YourPolicy2::onFree(void *address)
{
  // Here to be implemented
  MemoryStructure *ms = MemoryStructure::getInstance();

  for (int i = 0; i < ms->getAllocationListSize(); i++)
  {
    Allocation alloc = ms->getAllocation(i);
    if (alloc.addr == address)
    {
      ASSERT(ms->deallocate(address));
      return;
    }
  }

  printf("DE Error : allocation not found! (%lu, %p)\n", (unsigned long)address, address);
  exit(1);
}

static int defragmentPrepareEnd(int target_size)
{
  printf("*** Fast defragmentation!! Target Size : %d\n", target_size);
  std::this_thread::sleep_for(std::chrono::milliseconds(300));
  MemoryStructure *ms = MemoryStructure::getInstance();
  size_t last_cap = MAX_MEMORY_CAP;

  // Retry until threshold
  for (int search_length = ms->getAllocationListSize(); search_length; search_length--)
  {
    // Move while moving allocation succeeds
    for (bool succeeded = true; succeeded;)
    {
      succeeded = false;
      Allocation alloc_from = ms->getAllocation(search_length - 1);
      void *prev_end = 0;
      // Search for the space to move on
      for (int i = 0; i < search_length; i++)
      {
        Allocation alloc_front_to = ms->getAllocation(i);
        // Found the space
        if ((size_t)alloc_front_to.addr - (size_t)prev_end >= alloc_from.size)
        {
          ScheduleProcessor::getInstance()->notifyAddressChange(alloc_from.addr, prev_end);
          // Move allocation
          ASSERT(ms->migrate(alloc_from.addr, prev_end));
          succeeded = true;
          break;
        }
        prev_end = (void *)((size_t)alloc_front_to.addr + alloc_front_to.size);
      }
      // Check if the space is enough
      Allocation last_alloc = ms->getAllocation(search_length - 1);
      if (last_cap - (size_t)last_alloc.addr - last_alloc.size >= target_size)
      {
        // printf("good! %d at %lu\n", search_length - 1, (unsigned long)last_alloc.addr);
        return true;
      }
    }

    last_cap = (size_t)ms->getAllocation(search_length - 1).addr;
  }

  return false;
}

static int defragmentPrepareFront(int target_size)
{
  printf("*** Fast defragmentation failed. Slow defragmentation....\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(300));
  MemoryStructure *ms = MemoryStructure::getInstance();
  void *prev_end = 0;

  for (int i = 0; i < ms->getAllocationListSize(); i++)
  {
    Allocation alloc_from = ms->getAllocation(i);
    size_t rem_space = (size_t)alloc_from.addr - (size_t)prev_end;
    if (rem_space > 0)
    {
      if (rem_space >= target_size)
        return 1;
      ScheduleProcessor::getInstance()->notifyAddressChange(alloc_from.addr, prev_end);
      // Move allocation
      ASSERT(ms->migrate(alloc_from.addr, prev_end));
    }
    prev_end = (void *)((size_t)prev_end + alloc_from.size);
  }

  return 0;
}

static bool orderByRemainder(MemoryCandidate mc1, MemoryCandidate mc2)
{
  return mc1.remainder < mc2.remainder;
}

static int classOf(int size)
{
  if (size <= 4096)
    return 1;
  else if (size <= 1048576)
    return 2;
  else
    return 3;
}
