# Memory Management
## Policy 1: Best fit allocation
* The most tightly bounded free space should be allocated for given size.
* Since the least space is remained for each allocation, the fragmentation will be minimized just for that moment.
## Policy 2: Best fit in common pool allocation
* Similar sized free space should be allocated for given size according to each pool(0-4KB, 4KB-1MB, 1MB-16MB).
* Small allocation(from small pool) cannot affect large free space, which should be allocated for large allocation(from large pool).
* If there is no proper sized free space for given size, allocate by the previous Best Fit policy.
* In practical implementation, free space exploration time will decrease since free space will be managed according to each pool of common size, seperately. i.e. we can explore just one pool out of multiple(0-4KB, 4KB-1MB, 1MB-16MB) pool that includes given size.
## Evaluation
|     COST     |  First Fit  | Policy 1: Best FIt | Policy 2: Best Fit in common pool   |
|:------------:|:-----------:|:------------------:|:-----------------------------------:|
|    Random    | 23306661080 |     11962483435    |             11398822915             |
|    Greedy    |  2599053970 |     2599053970     |              3122111570             |
| Back & Forth | 31164454230 |     16138027800    |             14587587685             |
### Description
#### Random scheduling
* Two best fit algorithms were 2 times less cost than the first fit algorithm since the fragmentation hugely decreased.
#### Greedy scheduling
* First fit was the same with Best Fit since both did not generate fragmentation(no intermidate free)
* Best fit in common pool algorithm had greater cost than the others since my implementation was not able to handle a case that the malloc cannot find the same pool of free space even though there are lots of free space.
#### Back & Forth scheduling
* Two best fit algorithms were 2 times less cost than the first fit algorithm since the fragmentation was minimized.
* Best Fit in common pool algorithm was faster than the best fit algorithm since there was frequent memory free calls and it made lots of similar size of free spaces for each pool.
